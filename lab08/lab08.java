//Joseph Malisov
//CSE2 

import java.util.Random;
import java.util.Scanner;

public class lab08 {
  public static void main(String[] args) {
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(5) + 5; //Generates a random integer between 5 and 10 (inclusive)
    
    Scanner scan = new Scanner (System.in); // declares and initializes the Scanner.
    int numStudents = randomInt;
    
    String[] students = new String[numStudents];
    int[] midterm = new int[numStudents];
    
    System.out.println("Please enter " + numStudents + " student's names, pressing enter in between each one: ");
    for (int i = 0; i < numStudents; i++) {
      students[i] = scan.next();
    }
    for (int k = 0; k < numStudents; k++) {
      midterm[k] = randomGenerator.nextInt(100);
    }
    System.out.println("Here are the midterm grades for the students: ");
    for (int j = 0; j < numStudents; j++) {
      System.out.println( students[j] + ": " + midterm[j]);
      }
    }
  }
  