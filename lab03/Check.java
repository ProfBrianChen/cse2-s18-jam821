//This program inputs from the user the cost of a check, the percentage 
//tip they wish to pay, and how many ways to split it.
//It then calculates how much each person will need to spend in order to pay the check.
//Joseph Malisov
//Feb. 9, 2018
//CSE 002

import java.util.Scanner; 

public class Check{ //class
    			// main method required for every Java program
   			public static void main(String[] args) {  //main method

          Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN
          System.out.print("Enter the original cost of the check in the form xx.xx: "); 
                //prompts the user for the original cost of the check
          double checkCost = myScanner.nextDouble(); //converts input into a double
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):");
                //prompts the user for the tip percentage they wish to pay
          double tipPercent = myScanner.nextDouble();  //converts input into a double
          tipPercent /= 100; //We want to convert the percentage into a decimal value
          System.out.print("Enter the number of people who went out to dinner:");
                //prompts the user for the number of people who went out to dinner (how many ways to split the check)
          int numPeople = myScanner.nextInt(); //converts input into an integer
          
          double totalCost;
          double costPerPerson;
          int dollars,   //whole dollar amount of cost 
             dimes, pennies; //for storing digits to the right of the decimal point for the cost$
          totalCost = checkCost * (1 + tipPercent);
          costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
          dollars=(int)costPerPerson; //get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10;
          pennies=(int)(costPerPerson * 100) % 10;  
                //makes dimes and pennies go only to the hundreds place.
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); 
                //outputs how much each person owes





}  //end of main method   
  	} //end of class