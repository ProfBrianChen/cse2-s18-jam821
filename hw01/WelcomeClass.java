/// CSE 2 hw01
/// Joseph Malisov

public class WelcomeClass{
  
  public static void main(String[] args) {
    ///Prints "Welcome" to the terminal window
    System.out.println("   -----------");
    System.out.println("   | Welcome |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--A--M--8--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}