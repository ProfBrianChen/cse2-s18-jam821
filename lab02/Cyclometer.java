// Joseph Malisov
// Feb. 2, 2018
// CSE2
// My bicycle cyclometer (meant to measure speed, distance, etc.) 
// records two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during that time.
// In addition, it calculates distance of two trips using the given data.
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {

      //declaring variables
	  int secsTrip1=480;  //  total seconds of Trip 1
    int secsTrip2=3220;  // total seconds of Trip 2
		int countsTrip1=1561;  // total rotation count of Trip 1
		int countsTrip2=9037; // total rotation count of Trip 2
    double wheelDiameter=27.0,  // Diameter of the wheel
  	PI=3.14159, // Value of pi
  	feetPerMile=5280,  // Number of feet in a mile
  	inchesPerFoot=12,   // number of inches in a foot
  	secondsPerMinute=60;  // number of seconds in a minute.
	  double distanceTrip1, distanceTrip2,totalDistance; // declares variables for calculating distance of trips.
      
      //prints values
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); // ouputs Trip 1's minutes and rotation count.
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); // outputs Trip 2's minutes and rotation count.

      //calculating distances
	distanceTrip1=countsTrip1*wheelDiameter*PI; // Gives distance in inches
//(for each count, a rotation of the wheel travels
//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance of trip 1 in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance of trip 2 in miles 
	totalDistance=distanceTrip1+distanceTrip2; // total combined distance of trips

      //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles"); // prints distance of trip 1
	System.out.println("Trip 2 was "+distanceTrip2+" miles"); // prints distance of trip 2
	System.out.println("The total distance was "+totalDistance+" miles"); // prints total distance of combined trips

	}  //end of main method   
} //end of class