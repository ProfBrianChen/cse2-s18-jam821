//Joseph Malisov
//March 5, 2018
//CSE 2
//This program asks the user to input various information about 
//a course and checks that they inputed using the correct type.

import java.util.Scanner; //imports scanner

public class Hw05{
  
  public static void main(String[] args) {  //main method
    Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN

    //CLASS NAME
    System.out.print("Please enter a class name: "); //asks the user to input the name of a class.
      
    //checks for non-string inputs. Runs until user enters a string.
    while (!myScanner.hasNextLine() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: NEED INPUT OF STRING TYPE: "); //tells the user they entered the wrong type of information,
      //and asks to input again
      }
    
    String className = myScanner.nextLine(); //creates a variable for the user input of class name
   
    //DEPARTMENT NAME
    System.out.print("Please enter the department name: "); //asks the user to input the name of the department.
    
    //checks for non-string inputs. Runs until user enter a string.
    while (!myScanner.hasNextLine() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: NEED INPUT OF STRING TYPE: "); //tells the user they entered the wrong type of information,
      //and asks to input again
      }
    
    String deptName = myScanner.nextLine(); //creates a variable for the user input of department name.
   
    //TEACHER NAME
    System.out.print("Please enter the teacher's name: "); //asks the user to input the name of the teacher.
    
    //checks for non-string inputs. Runs until user enter a string.
    while (!myScanner.hasNextLine() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: NEED INPUT OF STRING TYPE: "); //tells the user they entered the wrong type of information,
      //and asks to input again
      }
    
    String teacher = myScanner.nextLine(); //creates a variable for the user input of teacher name.
   
    //COURSE NUMBER
    System.out.print("Please enter the course number: "); //asks the user for the input of a course number  
    
    //checks for non-integer inputs. Runs until user enters an integer.
    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
      //and asks to input again
      }
    
    int courseNum = myScanner.nextInt(); //creates a variable for the user input of course number
   
    //checks for non positive inputs. Runs until user enters a positive integer.
    while (courseNum < 1) {
       System.out.print("NOT VALID: PLEASE INPUT A POSITIVE INTEGER: "); //tells the user they entered a non-posotive integer,
      //and asks to input again
       while (!myScanner.hasNextInt() ) {
          String junkWord = myScanner.next();
          System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
         //and asks to input again.
       }
       courseNum = myScanner.nextInt(); // changes the variable value of course number once the user has entered a positive integer.
    }
    //MEETING PER WEEK
        System.out.print("Please enter the number of meetings per week: "); //asks the user for input of the number of meeting per week.   
    
    //checks for non-integer inputs. Runs until the user enters an integer.
    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
         //and asks to input again.
      }
    
    int meetings = myScanner.nextInt(); //creates a variable for the user input of meetings per week.
   
    //checks for non positive inputs. Runs until user enters a positive integer.
    while (meetings < 1) {
       System.out.print("NOT VALID: PLEASE INPUT A POSITIVE INTEGER: "); //tells the user they entered a non-posotive integer,
      //and asks to input again
       while (!myScanner.hasNextInt() ) {
          String junkWord = myScanner.next();
          System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
         //and asks to input again.
       }
       meetings = myScanner.nextInt(); //changes the variable value of meetings per week once the user has entered a positive integer.
    }
    
    //NUMBER OF STUDENTS
        System.out.print("Please enter the number of students in the class: "); //asks the user to input a positive integer for length   
    
    //checks for non-integer inputs. Runs until user enters an integer
    while (!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
         //and asks to input again.
        
    }
    int students = myScanner.nextInt(); //creates a variable for the user input of number of students in the class
    
    //checks for non-positive inputs. Runs until user enters a positive integer.
    while (students < 1) {
       System.out.print("NOT VALID: PLEASE INPUT A POSITIVE INTEGER: "); //tells the user they entered a non-posotive integer,
      //and asks to input again
       while (!myScanner.hasNextInt() ) {
          String junkWord = myScanner.next();
          System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
         //and asks to input again.
       }
       students = myScanner.nextInt(); //changes the variable value of students in the class once the user has entered a positive integer.
    }
    
    //CLASS TIME
        System.out.print("Please enter the class time (no colon): "); //asks the user to input a positive integer for the class time
    
    //checks for non-integer inputs. Runs until user enters an integer
    while (!myScanner.hasNextInt()) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
         //and asks to input again.
        
    }
    int classTime = myScanner.nextInt(); //creates a variable for the user input of number of class time
    
    //checks for non-positive inputs. Runs until user enters a positive integer.
    while (classTime < 1) {
       System.out.print("NOT VALID: PLEASE INPUT A POSITIVE INTEGER: "); //tells the user they entered a non-posotive integer,
      //and asks to input again
       while (!myScanner.hasNextInt() ) {
          String junkWord = myScanner.next();
          System.out.print("NOT VALID: NEED INPUT OF INTEGER TYPE: "); //tells the user they did not enter an integer,
         //and asks to input again.
       }
       classTime = myScanner.nextInt(); //changes the variable value of class time once the user has entered a positive integer.
    }
  }
}
