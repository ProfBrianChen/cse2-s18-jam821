//Joseph Malisov
//CSE2
//March 9, 2018
//This program encyptes an 'x' into a handful of stars. 
//The size of the x (and the number of stars) is determined by user input.

import java.util.Scanner; //imports scanner

public class encrypted_x{
  
  public static void main(String[] args) {  //main method
    Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN
 
    System.out.print("Please enter a positive ingeter for size of the sqaure: "); //asks the user to input a positive integer for length

    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter for size of the sqaure: "); //asks the user to input a positive integer for length
      }
    
    int input = myScanner.nextInt(); //creates a variable for the user input of twist length
      
    //checks for non positive inputs
    while ( (input < 1) || (input > 100) ) {
        System.out.print("NOT VALID: Please enter a positive ingeter for size of the square: "); //asks the user to input a positive integer for length
      while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter for size of the sqaure: "); //asks the user to input a positive integer for length
      }
      input = myScanner.nextInt();
    }
  
    int i = 0;
    int j = 0;
    int line = 0;
    while (i <= input){
      while (j <= input){
        if (j==line) {
          System.out.print(" ");
          j++ ;
          continue ;
        }
        if (j == (input-(line))) {
          System.out.print(" ") ;
          j++ ;
          continue;
        }
        System.out.print("*");
        j++ ;
      }
      i++ ;
      j = 0 ;
      System.out.println() ;
      line++ ;
    }
  }
}