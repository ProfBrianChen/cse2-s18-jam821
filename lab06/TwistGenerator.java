//Joseph Malisov
//CSE 2
//March 2, 2018
//This program asks the user for twist length and generates twists.

import java.util.Scanner; //imports scanner

public class TwistGenerator{
  
  public static void main(String[] args) {  //main method
    Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN

    System.out.print("Please enter a positive ingeter for length: "); //asks the user to input a positive integer for length
      
    //checks for non-integer inputs
    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter for length: "); //asks the user to input a positive integer for length
      }
    
    int length = myScanner.nextInt(); //creates a variable for the user input of twist length
   
    //checks for non positive inputs
    while (length < 1) {
        System.out.print("NOT VALID: Please enter a positive ingeter for length: "); //asks the user to input a positive integer for length
      while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter for length: "); //asks the user to input a positive integer for length
      }
      length = myScanner.nextInt();
    }
    
    //creating variables to be used for the calculations.
    int intlength = (int) length ;
    int goodLength = intlength - (intlength % 3);
    final int fLength = goodLength ;
    int length1 = fLength ;
    int length2 = fLength ;
    int length3 = fLength ;
    int remainder = (intlength % 3) ;
    int goodRemainder = (goodLength % 3);
    
        System.out.println("integer chosen = " + length); //prints the integer input by the user

    //PRINTING TWISTS
    //prints top line
    while (( (length1 / 2) > 0) && (goodRemainder == 0)) {
      System.out.print("\\ /") ;  
    length1 -= 3;
    }
    if ((remainder != 0)) {
      System.out.print ("\\") ;
    }
    
    System.out.println(""); //for spacing
    
    //prints x's
    while (((length2 / 2) > 0) && ((goodRemainder) == 0)) {
      System.out.print(" X ") ; 
      length2 -= 3;
    }
    if ((remainder > 1)) {
      System.out.print (" X ") ;
    }
    System.out.println("");
    
    //prints bottom line
    while (((length3 / 2) > 0) && ((goodRemainder) == 0)) {
      System.out.print("/ \\") ; 
      length3 -= 3; 
    }
    if ((remainder != 0)) {
      System.out.print ("/") ;
    }
    
    System.out.println(); //for spacing

    
  }
}
