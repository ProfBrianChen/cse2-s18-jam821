//Joseph Malisov
//CSE 2
//March 26, 2018
//This program calculates the area of a shape chosen by the user with deminsions also chosen by the user.

import java.util.Scanner; //imports Scanner

public class Area{ //declares class
  
  public static void main(String[] args) { //declares main method
    Scanner myScanner = new Scanner (System.in) ;  //declares scanner
    System.out.println("Please enter either 'rectangle', 'triangle', or 'circle': ") ; //asks the user to choose a shape for the area
    //following loops checks that the user input one of the allowed shapes
        String shape = myScanner.next(); //declares a variable for the desired shape.
    while ( (!shape.equals("rectangle") && (!shape.equals("circle")) && (!shape.equals("triangle")))) { //loop checks for the input of one of the options. If none were entered, asks user again.
      System.out.println("Please enter either 'rectangle', 'circle', or 'circle' without the apostrophes: ");
      shape = myScanner.next();
    }
    
    //IF THE USER ENTERS 'RECTANGLE'
    if (shape.equals("rectangle")) {
      System.out.println("Please enter the length of the rectangle: "); //asKs the user for the length of the rectangle
      double l = check(); //declares a variable for the length, and tells java to run the 'check' method in order to check that the user entered a positive number.
      System.out.println("Please enter the width of the rectangle: "); //asks the user for the width of the rectangle
      double w = check(); //declares a variable for the width, and tells java to run the 'check' method in order to check that the user entered a positive number.
      double area = areaR(l, w); //tells java to run the method for area of a rectangle with the given width and length.
    }
    
    //IF THE USER ENTERS 'TRIANGLE'
    else if (shape.equals("triangle")) {
      System.out.println("Please enter the height of the triangle: "); //asks the user for the length of the triangle
      double height = check(); //declares a variable for the height, and tells java to run the 'check' method in order to check that the user entered a positive number.
      System.out.println("Please enter the base length of the triangle: ");
      double base = check(); //declares a variable for the base, and tells java to run the 'check' method in order to check that the user entered a positive number.
      double area = areaT(height,base); //tells java to run the method for area of a triangle with the given base and height.
    }
    //IF THE USER ENTERS 'CIRCLE'
    else if (shape.equals("circle")) {
      System.out.println("Please enter the radius of the circle: "); //asks the user for the radius of the circle
      double r = check(); //declares a variable for the length, and tells java to run the 'check' method in order to check that the user entered a positive number.
      double area = areaC(r); //tells java to run the method for area of a circle with the given radius.
    }
  
  }
  //METHOD FOR CHECKING FOR POSITIVE INTEGERS
  public static double check () { //declares method for entering variables and checking that they are positive
    Scanner myScanner = new Scanner (System.in); //declares new scanner
    while (!myScanner.hasNextDouble()) { //loop checks that what they user entered is a double, and if it is not, prompts them to enter a positive number.
        System.out.println("Please enter a positive number: ");
        String junk = myScanner.next();
      }
    double a = myScanner.nextDouble(); //declares a variable to be returned from the method
    while ( a <= 0 ) { //loop checks that the entered value is positive and a number and if it is not, prompts them to enter a positive number.
      System.out.println("Please enter a positive number: ");
      while (!myScanner.hasNextDouble()) {
        System.out.println("Please enter a positive number: ");
        String Junk = myScanner.next();
      }
      a = myScanner.nextDouble() ;
    }
    return a; //returns the user's value (by now must be a positive number) to whereever it was called.
  }
  
  //METHOD FOR AREA OF A RECTANGLE
  public static double areaR (double l, double w) { //declares a method for area of a rectangle
    System.out.println("Area = " + l*w); //prints Area of rectangle
    return l*w ; //returns area of rectangle to wherever it was called from
  }
  
  //METHOD FOR AREA OF A TRIANGLE
  public static double areaT(double h, double b) { //declares a method for area of a triangle
    System.out.println("Area = " + .5*b*h); //prints area of triangle
    return .5*b*h; //returns area of triangle to wherever it was called from
  }
  //METHOD FOR AREA OF A CIRCLE
  public static double areaC(double r) { //declares a method for area of the circle
    System.out.println("Area = " + 3.14159*r); //prints area of circle
    return 3.14159*r; //returns area of circle to wherever it was called.
  }
}