//Joseph Malisov
//CSE 2
//March 26, 3018
//This code will check if the user's input is in letters.
//It can either check the whole input or up to the user's desired place in the input.

import java.util.Scanner; //imports scanner
public class StringAnalysis { //declares public class
  public static void main (String[] args) { //declares main method
    System.out.println("Please enter a string of characters: "); //prompts the user for a string of characters
    Scanner myScanner = new Scanner (System.in); // declares a scanner.
    while (!myScanner.hasNext()) { //loop for checking that the user entered a valid String.
      System.out.println("ERROR: Please enter a valid String: "); //prompts the user for another input
      String junk = myScanner.next();
    }
    String input = myScanner.next(); //declares a variable for the user input
    
    //asks the user if they would like to check if the input is letters only up to a certain point.
    System.out.println("If you would like to check if input is letters only up to certain point, enter 'yes'. If not, enter 'no'. ");
    
    int stopPoint = 0; //declares a variable for the stopping point
    if (myScanner.next().equals("yes")) { //checks for if the user entered 'yes' to checking only up to a certain point
      System.out.println("Please enter a positive integer: "); //prompts the user for a positive integer
      while (!myScanner.hasNextInt()) { //loop for checking if the entry is an integer
        System.out.println("Please enter a positive integer: ");
        String junk = myScanner.next();
      }
    stopPoint = myScanner.nextInt();
    while ( stopPoint <= 0 ) { // loop for checking if the entry is positive (and an integer).
      System.out.println("Please enter a positive integer: ");
      while (!myScanner.hasNextInt()) {
        System.out.println("Please enter a positive integer: ");
        String Junk = myScanner.next();
      }
      stopPoint = myScanner.nextInt() ; //changes the stopping point of checking to the user's entry.
    }
      
    }
    if (stopPoint > 0) { // if the user wants to just check all of the input, sends it to the method for checking all.
      boo(input, stopPoint);
    }
    else if (stopPoint ==0) { // if the user wants to check input up to a certain points, sends it to the method for just checking some of it
    boo(input);
    }
  }
  public static boolean boo(String a) { //declares method for checking all of input
    int z = 0; //declares variable for counting
    while (a.length() < z) { //loop for checking up to the user's entry
      if (Character.isLetter(a.charAt(z))) {
        z++;
      }
      if (!Character.isLetter(a.charAt(z))) { // if it runs into a non-letter, it will leave the loop.
        System.out.println("Not all are letters.");
        break;
      }
    }
    if (!Character.isLetter(a.charAt(z))) { //returns false and prints that not all are characters are letters (if that is true)
        System.out.println("Not all are letters.");
        return false;
    }
    System.out.println("All are letters."); //returns true  and prints that all characters are letters (if that is true)
    return true;
  }
  public static boolean boo(String a, int b) { //declares method for checking entry for letters up to the user's entry.
    int z = 0; //declares variable for counting up to the user's entry
    while (b > z) { //loop for checking letters up to the user's entry
      if (Character.isLetter(a.charAt(z))) {
        z++;
      }
      if (!Character.isLetter(a.charAt(z))) {//returns false and prints that not all are characters are letters (if that is true)
        System.out.println("Not all are letters.");
        return false;
      }
    }
    if (b==z) {//returns true and prints that all characters are letters (if that is true)
      System.out.println("All are letters.");
      return true;
    }
    return true;
  }
}
