public class BinarySearch{
  
  public static void main(String[] args){
    int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int target = 1;
    
    binary(array, target);
  }
  
  public static void binary(int[] array, int target){
    int max = array.length;
    int min = 0;
    while (min<=max){
      int mid = (max-min)/2 + min;
      if (array[mid]==target){
        System.out.println("Found!");
        return;
      }
      if (target > array[mid]){
        min = mid;
      }
      if (target < array[mid]){
        max = mid;
      }
    }
    System.out.println("Not Found!");
    return;
  }
}