//Room to test stuff from test 2
  import java.util.Scanner;

public class test2 {
  
  public static void main(String[] args) {
    Scanner myScanner = new Scanner (System.in) ;
    
    System.out.println("enter a positive int:");
    int input = myScanner.nextInt();
    
    pennant(input);
    
  }
  public static void pennant(int x) {
    if ( (x%2) == 0){
      System.out.println("Error: incorrect input.");
      return;
    }
    int i=1;
    for (int line=1; i <= line; i++){
      for (int y=x;y>0;y--){
        System.out.print("#");
      }
      System.out.print("@");
      if (line>=(x%2+1)){
        line--;
      }
      if (line<=(x%2+1)){
        line++;
      }
      System.out.println();
    }
  }
}