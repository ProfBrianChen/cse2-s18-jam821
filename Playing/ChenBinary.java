import java.util.Scanner;


public class ChenBinary {

	//ascending array
	public static void binarySearch(int[] list, int searchVal)
	{
		System.out.println("searching for " + searchVal);
		int start = 0;
		int stop = list.length;
		
		while(start < stop){
			System.out.println("Iterating the search");
			int mid = ( (stop - start) / 2 ) + start;
			if( list[mid] == searchVal ){
				System.out.println("Found the value: " + searchVal);
				break;
			}
			///we didnt find it, so update the range.
			if( searchVal > list[mid] ){
				start = mid+1; //updatge the bottom of the range
				System.out.println("searching upper half: [" + start + ", " + stop + "]");
			}
			if( searchVal < list[mid] ){
				stop = mid-1; //updatge the bottom of the range
				System.out.println("searching lower half: [" + start + ", " + stop + "]");
			}
		}
	}
	
	
	
	public static void main(String[] args)
	{
		
		int[] test = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		
		binarySearch(test, 1);
	}
	
	
}