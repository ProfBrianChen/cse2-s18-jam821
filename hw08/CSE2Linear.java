//Joseph Malisov
//CSE2
import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {
  public static void main(String[] args) {
    Scanner scan = new Scanner (System.in);
    int[] grades = new int[15];
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    for (int i=0; i<grades.length; i++){
      System.out.println("Please enter an integer between 0 and 100: ");
      grades[i] = scan.nextInt();
      if ( i>0 && grades[i] <= grades[i-1]) {
        System.out.println("Please enter an integer that is larger than the last");
        grades[i] = scan.nextInt();
      }
      if ( grades [i] > 100 || grades [i] < 0) {
        System.out.println("Please enter an integer within the range 0-100");
        grades[i] = scan.nextInt();
      }
    }
    System.out.println("Please enter an integer to search for: ");
    int search = scan.nextInt();
    binary(grades, search);
    System.out.println("Scrambled: ");
    scramble(grades);
    System.out.println();
    System.out.println("Please enter another integer to search for: ");
    int search2 = scan.nextInt();
    linear(grades, search2);
  }
  
  //BINARY SEARCH METHOD
  public static void binary(int[] grades, int i){ //declares method and input types. i is what the user is searching for
    int iterations = 1; // declares variable for iterations
    int x = 6; // declares variable to find spot in array
    while (i != grades[x]) { // loops until we find an item in the array is equal to the search UNLESS (see below)
      if (iterations>4){ // breaks the loop if we have looped more than 4 times (max to find something within an array of 15) and not found anything
        break;
      }
      if (i<grades[x]) { // if the search is less than current position, adds an iteration and shifts spot in array to halfway back
        iterations++;
        if (i==grades[x]) { //breaks to avoid changing our spot in the array once we have found the search integer
          break;
        }
        x= (x/2) ;
      }
      if (i>grades[x]) {// if the search is greater than current position, adds an iteration and shifts spot in array to halfway forward
        iterations++;
        if (i==grades[x]) {//breaks to avoid changing our spot in the array once we have found the search integer
          break;
        }
        x = x + ((grades.length - x)/2);
      }
    }
    if ( grades[x]==i ) {
      System.out.println("Found after " + iterations + " iterations"); //prints that search is found with however many iterations
    }
    else if (grades[x] != i) {
      System.out.println("Not found after " + iterations + " iterations"); //prints that search is not found with however many iterations (always 6)
    }
  }
  
  public static void linear(int grades[], int search2) {
    int iterations = 1;
    int x=0;
    for (; x < grades.length; x++, iterations++) {
      if (search2 == grades[x]) {
        break;
      }
    }
    if ( grades[x]==search2 ) {
      System.out.println("Found after " + iterations + " iterations"); //prints that search is found with however many iterations
    }
    else if (grades[x] != search2) {
      System.out.println("Not found after " + iterations + " iterations"); //prints that search is not found with however many iterations (always 15)
    }
  }
  //Method for scambling array
  public static int[] scramble(int[] grades) { //declares method and inputs
    for (int i=0; i<grades.length; i++) { // loop for going through each item in the array
	    int target = (int) ((grades.length-i)*Math.random() + i); // picks another random item in the array to draw from 
  	  int temp = grades[target];
	    grades[target] = grades[i];
	    grades[i] = temp;
      System.out.print(grades[i] + " ");
    }
    return grades;
  }
}