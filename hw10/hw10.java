//Joseph Malisov
//CSE2
//Due April 24, 2018
//This program simulated a grid city blocks, and robots taking them over
  //(indicated by negative values for the population of that block)
import java.util.Random; //imports Random class

public class hw10{
  public static void main (String[] args){ //declaration of main method
    Random random = new Random();
    int[][] array = buildCity();
    display(array);
    int robots = random.nextInt(19) + 1; //generates a random number of robots between 1 and 20.
    invade(array, robots);
    display(array);
    for (int i=0; i<5; i++) {
      update(array);
      display(array);
    }
  }
  
  public static int[][] buildCity(){ //method to build grid of random size
    Random random = new Random(); //declares a new random class
    int h = random.nextInt(5)+10; //declares and initializes a height for the grid
    int w = random.nextInt(5)+10; //declares and initializes a width for the grid
    int[][] cityArray = new int[w][h]; //declares and initializes a 2d array representing the grid using previously given height and width
    for (int i = 0; i<cityArray.length; i++){ // loops for entering values (populations) for city blocks going
      for (int j=0; j<cityArray[i].length; j++){
        cityArray[i][j] = random.nextInt(899)+100; //fills each block with a random integer between 100 and 999
      }
    }
    return cityArray; //returns the array as output from the method
  }
  
  public static void display(int[][] array){ //method for printing the grid
    for (int i=0; i<array.length; i++){ //outer loop for printing grid
      for (int j=0; j<array[i].length; j++){ //inner loop for printing grid
        System.out.printf(array[i][j] + " "); //prints each individual block value (population)
      }
      System.out.println(); //starts new rows to keep city in a grid.
    }
    System.out.println();
  }
  
  public static int[][] invade(int[][] array, int k){
    Random random = new Random(); //declares a new random class
    int[][] arrayK = new int[k][k]; //array for random coordinates for robots to land
  /*  for (int i=0; i<k; i++){ //outer loop for setting values in the the array for robot coordinates
      for (int j=0; j<k; j++){ //inner loop for setting values in the the array for robot coordinates
        arrayK[i][j] = random.nextInt(array.length); //sets a random value in the 2d array of random coordinates for the robots to land
      }
    }
    */
    while ( k>0){
      int a = random.nextInt(array.length);
      int b = random.nextInt(array[1].length);
      if (array[a][b] < 0) { //if this block is already negative (occupied by a robot), while loop tries again.
        continue;
      }
      array[a][b] = -(array[a][b]);
      k--;
    }
    return array;
  }
  
  public static int[][] update(int[][] array){
    int y =0;
    for (int i=0; i<array.length; i++){
      for (int j=0; j<array[1].length; j++){
        if (array[i][j] < 0){ //finds values that are negative (where robots reside)
          array[i][j] = -(array[i][j]); //sets previously negative value to positive (robot moves away)
          if (j < array.length && j>0){ //makes sure it does not move robot east past city limits (if limit is not reached, next value is turned negative)
            array[i][j-1] = -(array[i][j-1]);
          }
        }
      }
    }
    return array;
  }
}