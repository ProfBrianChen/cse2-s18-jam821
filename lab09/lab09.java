//Joseph Malisov
//CSE 2 Lab 09

public class lab09 {
  public static void main (String[] args){
    int[] array0 = {0, 1, 2, 3, 4, 5, 6, 7};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
  
  public static int[] copy( int[] list ){
    int[] myList = new int[list.length];
    for (int i=0; i<list.length; i++){
      myList[i]=list[i];
    }
    return myList;
  }
  public static void inverter(int[] list){
    int[] temp = new int[list.length];
    for (int i=0; i<list.length; i++){
      temp[i] = list[i];
    }
    for (int i=0; i<list.length; i++){
      list[i] = temp[temp.length-1-i];
    }
  }
  public static int[] inverter2(int[] list){
    int[] list2 = copy(list);
    inverter(list2);
    return list2;
  }
  public static void print(int[] list){
    System.out.println("Array: ");
    for (int i=0; i<list.length; i++){
      System.out.print(list[i] + " ");
    }
    System.out.println();
  }
}