//Joseph Malisov
//CSE 002
//April 20, 2018
  import java.util.Random;

public class lab10{
  public static void main(String[] args){
    Random random = new Random();
    int w1 = random.nextInt(5)+2;
    System.out.println(w1);
    int h1 = random.nextInt(5)+2;
    System.out.println(h1);
    int w2 = random.nextInt(5)+2;
    System.out.println(w2);
    int h2 = random.nextInt(5)+2;
    System.out.println(h2);
    int[][] arrayA = increasingMatrix(w1, w1, true);
    int[][] arrayB = increasingMatrix(w1, w1, false);
    int[][] arrayC = increasingMatrix(w2, w2, true);
    System.out.println("arrayA: ");
    printMatrix(arrayA, true);
    System.out.println("arrayB: ");
    printMatrix(arrayB, true);
    System.out.println("arrayC: ");
    printMatrix(arrayC, true);
    int [][] addedArrayAB = addMatrix(arrayA, true, arrayB, false);
    System.out.println("added arrays A+B: ");
    printMatrix(addedArrayAB, true);
    int [][] addedArrayAC = addMatrix(arrayA, true, arrayC, true);
    System.out.println("added arrays A+C: ");
    printMatrix(addedArrayAC, true);
  }
  
  public static int[][] increasingMatrix(int height, int width, boolean format){
    int[][] array = new int [height][width];
    int k = 1;
    if (format){
      for (int i=0; i<width; i++) {
          for (int j=0; j<height; j++) {
            array[j][i] = k;
            k++;
          }
        }
    }
    else {
        for (int i=0; i<height; i++) {
          for (int j=0; j<width; j++) {
            array[i][j] = k;
            k++;
          }
        }
    }
    System.out.println("array[0]: " + array.length);
    return array;
  }
  
  public static void printMatrix(int[][] array, boolean format){
    if (array != null){
      System.out.println("array members: " + array.length);
      for (int i=0; i<array.length; i++) {
        for (int j=0; j<array[0].length; j++) {
          System.out.print(array[j][i] + " ");
        }
        System.out.println();
      }
    }
    else {
      System.out.println("The array is empty!");
    }
  }
  
  public static int[][] translate(int[][] array){
    int[][] translated = new int[array.length][array[1].length];
    int k=1;
    for (int i = 0; i<array.length; i++){
      for (int j =0; j<array[1].length; j++){
        array[j][i]= k ;
        k++;
      }
    }
    System.out.println("Print translated array: ");
    for (int i=0; i<array.length; i++){
      for (int j=0; j<array[1].length; j++){
        System.out.print(array[j][i] + " ");
      }
      System.out.println();
    }
    return array;
  }
  
  public static int[][] addMatrix( int[][] a, boolean formata, int[][] b, boolean formatb) {
    if (a.length!=b.length || a[1].length != b[1].length){
      System.out.println("Matices cannot be added!");
      return null;
    }
    int[][] added = new int[a.length][a[1].length];
    if (!formata) {
      a = translate(a);
    }
    if (!formatb) {
      b = translate(b);
    }
    for (int i = 0; i<added.length; i++){
      for (int j=0; j<added[1].length; j++){
        added[i][j]=a[i][j]+b[i][j];
      }
    }
    return added;
  }
}