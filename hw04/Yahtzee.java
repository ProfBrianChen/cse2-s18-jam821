//Joseph Malisov
//CSE 2
//Feb. 19, 2018
//This program simulates a Yahtzee roll, and adds up the points 
import java.util.Scanner; //imports scanner

public class Yahtzee{
    			// main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN
    
    System.out.print("If you would like to enter your own roll, please enter it in the form of a single 5 digit number: "); 
                //prompts the user if they would like to enter their own dice roll.
    
        
    double userd = myScanner.nextDouble(); //creates a variable for the user input of die rolls
    
    //changes variables for dice rolls according to user input
    int d1 = (int) (userd/10000);
    int d2 = (int) ((userd%10000)/1000);
    int d3 = (int) ((userd%1000)/100);
    int d4 = (int) ((userd%100)/10);
    int d5 = (int) ((userd%10)/1);
    
    //Tests for a valid user entry
    if ((userd > 66666) || (d1<1 || d1>6) || (d2<1 || d2>6) || (d3<1 || d3>6) || (d4<1 || d4>6) || (d5<0 || d5>6)) {
      System.out.println("NO OR IMPROPER ENTRY - RANDOM INPUT WILL BE USED."); //if the user made an invalid entry, this tells them so.
      //assuming no or improper entry, dice rolls are created randomly:
      d1 = (int) (Math.random()*6+1); //Creates a random variable for rolling die #1
      d2 = (int) (Math.random()*6+1);//Creates a random variable for rolling die #2
      d3 = (int) (Math.random()*6+1); //Creates a random variable for rolling die #3
      d4 = (int) (Math.random()*6+1); //Creates a random variable for rolling die #4
      d5 = (int) (Math.random()*6+1); //Creates a random variable for rolling die #5
    }
    
    
    //UPPER SECTION
    // creates variables for the number of each type of die
    int aces = 0 ;
    int twos = 0 ;
    int threes = 0 ;
    int fours = 0 ;
    int fives = 0 ;
    int sixes = 0 ;
      
    //LINES 47-115 JUST TAKE THE DICE ROLLS AND ADD UP EACH TYPE
    switch (d1) {
      case 1: ++aces;
        break;
      case 2: ++twos;
        break;
      case 3: ++threes;
        break;
      case 4: ++fours;
        break;
      case 5: ++fives;
        break;
      case 6: ++sixes;
        break;
    }
    switch (d2) {
      case 1: ++aces;
        break;
      case 2: ++twos;
        break;
      case 3: ++threes;
        break;
      case 4: ++fours;
        break;
      case 5: ++fives;
        break;
      case 6: ++sixes;
        break;
    }
    switch (d3) {
      case 1: ++aces;
        break;
      case 2: ++twos;
        break;
      case 3: ++threes;
        break;
      case 4: ++fours;
        break;
      case 5: ++fives;
        break;
      case 6: ++sixes;
        break;
    }
    switch (d4) {
      case 1: ++aces;
        break;
      case 2: ++twos;
        break;
      case 3: ++threes;
        break;
      case 4: ++fours;
        break;
      case 5: ++fives;
        break;
      case 6: ++sixes;
        break;
    }
    switch (d5) {
      case 1: ++aces;
        break;
      case 2: ++twos;
        break;
      case 3: ++threes;
        break;
      case 4: ++fours;
        break;
      case 5: ++fives;
        break;
      case 6: ++sixes;
        break;
    }
    
    //LINES 119-124 PRINT OUT HOW MANY OF EACH VALUE WAS ROLLED 
    System.out.println("Aces: " + aces);
    System.out.println(" Twos: " + twos);
    System.out.println(" Threes: " + threes);
    System.out.println(" Fours: " + fours);
    System.out.println(" Fives: " + fives);
    System.out.println(" Sixes: " + sixes);

    //Total upper section initial points
    int ipoints = aces + twos*2 + threes*3 + fours*4 + fives*5 + sixes*6 ; 
    System.out.println("Total upper section points before bonus: " + ipoints);
    
    //Total points from the upper round
    int tpoints = ipoints;
    if (ipoints >= 63) {
      tpoints = ipoints + 35;
    }
    System.out.println("Total upper section points with bonus: " + tpoints);
  
    // LOWER SECTION
    
    int lowerpoints = 0; //creates a variable for lower section points
    //lines 141-187 are for 3 of a kind
    switch (aces) { // adds points for 3 aces, 4 aces, or 5 aces (Yahtzee)
      case 3: lowerpoints += aces;
        break;
      case 4: lowerpoints += aces;
        break;
      case 5: lowerpoints += 50;
        break;
    }
    switch (twos) { //adds points for 3 2's, 4 2's, or 5 2's (Yahtzee)
      case 3: lowerpoints += twos*2;
        break;
      case 4: lowerpoints += twos*2;
        break;
      case 5: lowerpoints += 50;
        break;
    }
    switch (threes) { //adds points for 3 3's, 4 3's, or 5 3's (Yahtzee)
      case 3: lowerpoints += threes*3;
        break;
      case 4: lowerpoints += threes*3;
        break;
      case 5: lowerpoints += 50;
        break;
    }
    switch (fours) { //adds points for 3 4's, 4, 4's, or 5 4's (Yahtzee)
      case 3: lowerpoints += fours*4;
        break;
      case 4: lowerpoints += fours*4;
        break;
      case 5: lowerpoints += 50;
        break;
    }
    switch (fives) { //adds points for 3 5's, 4 5's, or 5's (Yahtzee)
      case 3: lowerpoints += fives*5;
        break;
      case 4: lowerpoints += fives*5;
        break;
      case 5: lowerpoints += 50;
        break;
    }
    switch (sixes) { //adds points for 3 6's, 4 6's, or 5 6's (Yahtzee)
      case 3: lowerpoints += sixes*6;
        break;
      case 4: lowerpoints += sixes*6;
        break;
      case 5: lowerpoints += 50;
        break;
    }
    
    //full house
    if (((aces == 3) || (twos == 3) || (threes == 3) || (fours == 3) || (fives == 3) || (sixes == 3)) && ((aces == 2) || (twos == 2) || (threes == 2) || (fours == 2) || (fives == 2) || (sixes == 2))) {
      lowerpoints += 25; //adds 25 points to the lowerpoints total if conditions are met for a full house
    }
    
    //small straight
    if ((aces >= 1 && twos >=1 && threes >=1 && fours >=1) || (twos >= 1 && threes >=1 && fours >=1 && fives >=1) || (threes >= 1 && fours >=1 && fives >=1 && sixes >=1)){
      lowerpoints += 30; // adds 30 points to the lower points total if conditions are met for a small straight
    }
    
    //large straight
    if ((aces >= 1 && twos >=1 && threes >=1 && fours >=1 && fives >=1) || (twos >= 1 && threes >=1 && fours >=1 && fives >=1 && sixes >=1)){
      lowerpoints += 10; //adds 10 points to the lower points total if conditions are met for a large straight (only adds 10 because it already counts the small straight)
    }
    
    //addition for 'chance' section
    lowerpoints += ipoints;
    
    //prints lower half total
    System.out.println("Lower Section total: " + lowerpoints);
    //prints lower half total + upper half total:
    System.out.println("TOTAL POINTS: " + (lowerpoints + tpoints));


  }
}