//Joseph Malisov
//CSE2
//March 20, 2018
//This program creates a pattern of Argyle diamonds with some user input

import java.util.Scanner; //imports scanner

public class Argyle{
  
  public static void main(String[] args) {  //main method
    Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN
 
    System.out.print("Please enter a positive integer for the height of viewing window in characters: "); //asks the user to input a positive integer for length

    //WIDTH OF VIEWING WINDOW
    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      }
    
    int windowHeight = myScanner.nextInt(); //creates a variable for the user input of window Height
      
    //checks for non positive inputs
    while ( (windowHeight < 1) ) {
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      }
      windowHeight = myScanner.nextInt();
    }
    
    //LENGTH OF VIEWING WINDOW
    System.out.print("Please enter a positive integer for the length of viewing window in characters: "); //asks the user to input a positive integer for length

    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      }
    
    int windowLength = myScanner.nextInt(); //creates a variable for the user input of window Length
      
    //checks for non positive inputs
    while ( (windowLength < 1) ) {
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      }
      windowLength = myScanner.nextInt();
    }
  
    //WIDTH OF ARGYLE DIAMONDS
    System.out.print("Please enter a positive integer for the width of the argyle diamonds: "); //asks the user to input a positive integer for length

    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      }
    
    int diamondWidth = myScanner.nextInt(); //creates a variable for the user input of diamond width
      
    //checks for non positive inputs
    while ( (diamondWidth < 1) ) {
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      }
      diamondWidth = myScanner.nextInt();
    }
			diamondWidth--;
    //WIDTH OF ARGYLE CENTER STRIPE
    System.out.print("Please enter a positive odd integer for the width of the argyle center stripe: "); //asks the user to input a positive integer for length

    while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive ingeter: "); //asks the user to input a positive integer for length
      }
    
    int centerWidth = myScanner.nextInt(); //creates a variable for the user input of center width
      
    //checks for non positive inputs
    while ( (centerWidth < 1) || (centerWidth > 100) || ((centerWidth % 2) ==0) ) {
        System.out.print("NOT VALID: Please enter a positive odd ingeter: "); //asks the user to input a positive integer for length
      while (!myScanner.hasNextInt() ) {
        String junkWord = myScanner.next();
        System.out.print("NOT VALID: Please enter a positive odd ingeter: "); //asks the user to input a positive integer for length
      }
      centerWidth = myScanner.nextInt();
    }
    
      
    //1ST CHARACTER FOR PATTERN
    System.out.print("Please enter a first character for the pattern fill: "); //asks the user to input a positive integer for length

    Scanner myScanner2 = new Scanner(System.in);
		String temp = myScanner.next();
		char pattern1 = temp.charAt(0);
    //2ND CHARACTER FOR PATTERN
    System.out.print("Please enter a second character for the pattern fill: "); //asks the user to input a positive integer for length

		String temp2 = myScanner.next();
		char pattern2 = temp2.charAt(0);

    //3RD CHARACTER FOR PATTERN
    System.out.print("Please enter a third character for the pattern fill: "); //asks the user to input a positive integer for length

		String temp3 = myScanner.next();
		char pattern3 = temp3.charAt(0);

    //LOOPS FOR PRINTING ARGYLES
    int k = 0; //creates a variable for the character number in a single line (tracks width of window)
		int j = 0; //creates a variable for the character number in each repeition of the pattern.
    int p = 0; //creates a variable to track the height of the window.
    int repNum = 1 ; //Sets a variable for number of repititions in the x direction.
    int line = 0;
    int linePastHalf = (diamondWidth/2) ;
      while (line <= diamondWidth){ //loop for length of diamond
        while (p < windowHeight) { //loop for height of the window
          while (k <= windowLength){ //loop for window length
            if ( j >= (line-centerWidth/2) && (j<= line+centerWidth/2) ) {
              System.out.print(pattern1);
              j++ ;
              k++ ;
							if ( (j % diamondWidth) == 0 ) {
              j= 0 ;
            	}
              continue ;
            }
            if ( (j >= ((diamondWidth-line)-centerWidth/2)) && (j <= (diamondWidth-line)+centerWidth/2)) {
              System.out.print(pattern1) ;
              j++ ;
              k++;
              if ( (j % diamondWidth) == 0 ) {
              j= 0 ;
            	}
              continue;
            }
            if ( (line < diamondWidth/2) && ((j < (diamondWidth/2 - line)) || (j > (diamondWidth/2 + line)))) {
              System.out.print(pattern3);
              j++;
              k++;
              if ( (j % diamondWidth) == 0 ) {
              	j= 0 ;
            	}
              continue;
            }

            if ( (line > diamondWidth/2) && ((j < ((diamondWidth/2) - (linePastHalf))) || (j > (diamondWidth/2) + linePastHalf))) {
              System.out.print(pattern3);
              j++;
              k++;
              if ( (j % diamondWidth) == 0 ) {
              	j= 0 ;
            	}
              continue;
            }
            System.out.print(pattern2);
            j++ ;
            k++ ;
            if ( (j % diamondWidth) == 0 ) {
              j= 0 ;
            }
          }
          k = 0;
          j = 0 ;
          line++ ;
          if (line > diamondWidth/2) {
            linePastHalf-- ;
          }
          p++ ;
          repNum = 1 ;
          System.out.println() ;
          if (line >= diamondWidth) {
            line = 0 ;
            linePastHalf = (diamondWidth/2) ;
          }
        }
     } 
  }
}