//Joseph Malisov
//Feb 5, 2018
//CSE 002 - hw2 - "Arithmetic"
// This program calculates and displays the total prices (with and without tax) on items bought in a store in Pennsylvania.

public class Arithmetic{
  
  public static void main(String[] args) {
    
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per box of envelopes
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

double totalCostOfPants = (double) numPants * (double) pantsPrice;   // total cost of pants
double totalCostOfShirts = (double) numShirts * (double) shirtPrice;  // total cost of sweatshirts
double totalCostOfBelts = (double) numBelts * (double) beltCost;   // total cost of belts
double PantsTax = (int) ((double)(totalCostOfPants * paSalesTax * 100)) / 100.0; //Tax on all pants bought
double ShirtsTax = (int) ((double) (totalCostOfShirts * paSalesTax * 100)) / 100.0; //Tax on all shirts bought
double BeltsTax = (int) ((double) (totalCostOfBelts * paSalesTax * 100)) / 100.0; //Tax on all belts bought
double TotalNoTax = (int) ((double) (totalCostOfPants + totalCostOfShirts + totalCostOfBelts ) *100) / 100.0; //Total cost without tax
double TotalYesTax = (int) ((double) (TotalNoTax + (TotalNoTax * paSalesTax)) *100) / 100.0; //Total cost with tax

System.out.println("Total Cost of Pants: " + totalCostOfPants); //prints total cost of pants
System.out.println("Total Cost of Shirts: " + totalCostOfShirts); //prints total cost of shirts
System.out.println("Total Cost of Belts: " + totalCostOfBelts); //prints total cost of belts
System.out.println("Total Tax of Pants: " + PantsTax); //prints total tax of pants
System.out.println("Total Tax of Shirts: " + ShirtsTax); //prints total tax of shirts
System.out.println("Total Tax of Belts: " + BeltsTax); //prints total tax of belts
System.out.println("Total Price Without Tax: " + TotalNoTax); //prints total price without tax
System.out.println("Total Price With Tax: " + TotalYesTax); //prints total price with tax


  }
}
