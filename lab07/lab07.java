//Joseph Malisov
//CSE 2
//This program randomly generates sentences

import java.util.Random;

public class lab07 {
  
  public static void main (String[] args) {
    //TOPIC SENTENCE
    String noun = noun();
    System.out.println("The " + adjective() + " " + noun + " " + verb() + " the " + adjective() + " " + noun() + ". ");
    
    int z = randomNum();
    int y = 0;
    for (; y < z; y++) {
      System.out.print("That " + noun + " " + verb() + " its " + noun() + "s. It used " + noun() + "s as it " + verb() + " " + noun() + " at the " + adjective() + " " + noun() + ". ");
    }
  }
  //METHOD FOR RANDOM NUMBER GENERATION
  public static int randomNum () {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10) + 1;
    return randomInt;
  }
  
  //METHOD FOR ADJECTIVE GENERATION
  public static String adjective() {
    String a = "";
    int z = randomNum();
    switch (z) {
      case 1:
        a = "big";
        break;
      case 2:
        a = "small";
        break;
      case 3:
        a = "weird";
        break;
      case 4: 
        a = "cool";
        break;
      case 5:
        a = "brisk";
        break;
      case 6:
        a = "white";
        break;
      case 7:
        a = "smart";
        break;
      case 8:
        a = "silly";
        break;
      case 9:
        a = "crazy";
        break;
      case 10: 
        a = "wet";
        break;
    }
    return a;
  }
  
  //METHOD FOR NOUN GENERATION
  public static String noun() {
    String a = "";
    int z = randomNum();
    switch (z) {
      case 1:
        a = "dog";
        break;
      case 2:
        a = "cat";
        break;
      case 3:
        a = "table";
        break;
      case 4: 
        a = "cup";
        break;
      case 5:
        a = "rug";
        break;
      case 6:
        a = "sock";
        break;
      case 7:
        a = "shirt";
        break;
      case 8:
        a = "ball";
        break;
      case 9:
        a = "hand";
        break;
      case 10: 
        a = "catle";
        break;
    }
    return a;
  }
  
  //METHOD FOR VERB GENERATION
  public static String verb() {
    String a = "";
    int z = randomNum();
    switch (z) {
      case 1:
        a = "crushed";
        break;
      case 2:
        a = "walked";
        break;
      case 3:
        a = "destroyed";
        break;
      case 4: 
        a = "played";
        break;
      case 5:
        a = "protruded";
        break;
      case 6:
        a = "sailed";
        break;
      case 7:
        a = "sank";
        break;
      case 8:
        a = "drank";
        break;
      case 9:
        a = "ate";
        break;
      case 10: 
        a = "slapped";
        break;
    }
    return a;
  }
}