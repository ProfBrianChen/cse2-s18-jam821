//Joseph Malisov
//CSE 002
//April 16, 2018
import java.util.Random;

public class DrawPoker{
  public static void main(String[] args){
    int[] deck = new int[51];
    for (int i=0; i<deck.length; i++){ //sets the array equal to a number from 1 to 52 (one for each card in the deck).
      deck[i] = i ;
    } 
    for (int i=0; i<deck.length; i++) { //'shuffles the deck' (scatters the array)
	    int target = (int) ( (deck.length-i) * Math.random() + i);
	    int temp = deck[target];
	    deck[target] = deck[i];
	    deck[i] = temp;
      System.out.print(deck[i] + " ");
    }
		System.out.println(); //line break for spacing
		int[] hand1 = new int[5];
		System.out.println("Player One's Hand: ");
		for (int i=0; i<5; i++) { //loop for printing player one's hand
			hand1[i] = deck[2*i];
			int suit = hand1[i]/13 ;
			//FOLLOWING IF STATEMENTS PRINT OUT SUITS
			if ( suit == 0) {
				System.out.print("Diamond: ");
			}
			if ( suit == 1){
				System.out.print("Hearts: ");
			}
			if ( suit == 2) {
				System.out.print("Clubs: ");
			}
			if ( suit == 3){
				System.out.print("Spaids: ");
			}
			System.out.print(hand1[i]%13 + " | "); // prints out card number (from 0 to 12)
		}		
		System.out.println(); //line break for spacing
		int[] hand2 = new int[5]; //delcares player two's hand and allocates space for it
		System.out.println("Player Two's Hand: ");
		for (int i=0; i<5; i++) { //loop for printing player two's hand
			hand2[i] = deck[2*i+1];
			int suit = hand2[i]/13 ;
			//FOLLOWING IF STATEMENTS PRINT OUT SUITS
			if ( suit == 0) {
				System.out.print("Diamond: ");
			}
			if ( suit == 1){
				System.out.print("Hearts: ");
			}
			if ( suit == 2) {
				System.out.print("Clubs: ");
			}
			if ( suit == 3){
				System.out.print("Spaids: ");
			}
			System.out.print(hand2[i]%13 + " | "); // prints out card number (from 0 to 12)
		}		
		
		System.out.println(); //line break for spacing
		
		//PRINTS STATEMENTS ABOUT EACH PLAYERS HANDS USING METHODS BELOW
		System.out.println("Does player 1 have any pairs?"); 
		if (pair(hand1)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		}
		
		System.out.println("Does player 2 have any pairs?");
		if (pair(hand2)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		}
		
		System.out.println("Does player 1 have 3 of a kind?");
		if (triple(hand1)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		}
		
		System.out.println("Does player 2 have 3 of a kind?");
		if (triple(hand2)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		
		}
		System.out.println("Does player 1 have a flush?");
		if (flush(hand1)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		}
		
		System.out.println("Does player 2 have a flush?");
		if (flush(hand2)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		}
		System.out.println("Does player 1 have a full house?");
		if (fullHouse(hand1)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		}
		
		System.out.println("Does player 2 have a full House?");
		if (fullHouse(hand2)) {
			System.out.println("Yes!");
		}
		else {
			System.out.println("No.");
		}
		
		//DECIDES WINNER. STARTS AT HIGHEST (FULL HAND), THEN WORKS ITS WAY DOWN THROUGH ELSE IF STATEMENTS
		if (fullHouse(hand1) && !fullHouse(hand2)){
			System.out.println("Player 1 wins by full house!");
		}
		else if (!fullHouse(hand1) && fullHouse(hand2)){
			System.out.println("Player 2 wins by full house!");
		}
		else if (fullHouse(hand1) && fullHouse(hand2)){
			System.out.println("Tie by full house!");
		}
		else if (flush(hand1) && !flush(hand2)){
			System.out.println("Player 1 wins by flush!");
		}
		else if (!flush(hand1) && flush(hand2)){
			System.out.println("Player 2 wins by flush!");
		}
		else if (flush(hand1) && flush(hand2)){
			System.out.println("Tie by flush!");
		}
		else if (triple(hand1) && !triple(hand2)){
			System.out.println("Player 1 wins by three of a kind!");
		}
		else if (!triple(hand1) && triple(hand2)){
			System.out.println("Player 2 wins by three of a kind!");
		}
		else if (triple(hand1) && triple(hand2)){
			System.out.println("Tie by three of a kind!");
		}
		else if (pair(hand1) && !pair(hand2)){
			System.out.println("Player 1 wins by pair!");
		}
		else if (!pair(hand1) && pair(hand2)){
			System.out.println("Player 2 wins by pair!");
		}
		else if (pair(hand1) && pair(hand2)){
			System.out.println("Tie by pair!");
		}
		else if (!pair(hand1) && !pair(hand2)){
			System.out.println("Tie by High Card!");
		}
  }
	
	public static boolean pair(int[] hand){ // CHECKING FOR PAIRS
		for (int i=0; i<5; i++){//loop for going through each card in the array
			for (int j=0; j<5; j++) {// loop for looking at the other card number in the array
				if (i != j) {//makes sure it doesn't count itself as a pair
					if ((hand[i]%13)==(hand[j]%13)) {//if it does find the same number card in the array (a pair), then it returns true
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean triple(int[] hand){ //CHECKS FOR THREE OF A KIND
		for (int i=0; i<5; i++){ //loop for going through each card in the array
			for (int j=0; j<5; j++) { // loop for looking at the other card number in the array
				if (i != j) { //makes sure it doesn't count itself as a pair
					if ((hand[i]%13)==(hand[j]%13)) { //if it does find the same number card in the array (a pair), then it runs the following loop
						for (int k=0; k<5; k++){ // now this loop checks for a third card to have the same value.
							if ((k != i) && (k != j)) { //forces loop to skip choosing itself as equivalent.
								if (((hand[k]%13) == (hand[j]%13)) || (hand[k]%13) == (hand[i]%13)){ //if this new card is equivalent to one of the array 
									//members discovered earlier in the loop, then it must be a three of kind
									return true; 
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	public static boolean flush(int[] hand){ //CHECKS FOR A FLUSH
		if ( (hand[0]/13 == hand[1]/13) && (hand[0]/13 == hand[2]/13) &&
				(hand[0]/13 == hand[3]/13) && (hand[0]/13 == hand[4]/13) ){
			return true; //if all of the suits are the same, method returns true.
		}
		return false;
	}
	
	public static boolean fullHouse(int[] hand){ //CHECKS FOR A FULL HOUSE
		for (int i=0; i<5; i++){ //loop for going through each card in the array
			for (int j=0; j<5; j++) { // loop for looking at the other card number in the array
				if (i != j) { //makes sure it doesn't count itself as a pair
					if ((hand[i]%13)==(hand[j]%13)) { //if it does find the same number card in the array (a pair), then it runs the following loop
						for (int k=0; k<5; k++){ // now this loop checks for a third card to have the same value.
							if ((k != i) && (k != j)) { //forces loop to skip choosing itself as equivalent.
								if (((hand[k]%13) == (hand[j]%13)) || (hand[k]%13) == (hand[i]%13)){ //if this new card is equivalent to one of the array 
									//members discovered earlier in the loop, then it must be a three of kind
									//SINCE WE KNOW THERE IS A TRIPLE, WE CAN NOW CHECK FOR THE OTHER TWO TO BE A PAIR
									for (int z =0; z<5; z++){ //basically a new loop for checking the other two numbers against each other for a pair
										for (int y=0; y<5; y++){ //finds the other of the two last cards.
											if ( ( (z != i) && (z != j) && (z != k)) && (hand[y]%13==hand[z]%13)) { //compares the values of the two last cards.
												//if they are equivalent, prints true.
												return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}