//Joseph Malisov
//CSE 2
//Feb 10, 2018
//This program prompts the user for dimensions of a pyramid and returns the output

import java.util.Scanner; //imports scanner

public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN

          System.out.print("Enter enter the base side length: "); 
                //prompts the user for the base side length
          double side = myScanner.nextDouble(); //creates a variable for the input of the side length
               
          System.out.print("Enter enter the height: "); 
                //prompts the user for the height
          double h = myScanner.nextDouble(); //creates a variable for the input of the height

          double v = side * side * h / 3 ; //squares the side length, multiplies it by the beight, 
            //and then divides by 3 to get the volume of the pyramid
          
         System.out.println("The volume of the pyramid is: " + v); 
            //prints the volume of the pyramid

          
}  //end of main method   
  	} //end of class