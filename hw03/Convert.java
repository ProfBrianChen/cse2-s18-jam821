//Joseph Malisov
//CSE 2
//Feb. 9, 2018
//This program asks the user how many acres of land are affected
//by a hurricane and how many inches of rain were dropped on average.
//It then converts the quantity of rain to cubic miles.

import java.util.Scanner; //imports scanner

public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in ); //creating an instance that will take input from STDIN
          System.out.print("Enter enter the number of acres affected by the hurricane: "); 
                //prompts the user for the number of acres of land affected by the hurricane.
          double acres = myScanner.nextDouble(); //creates a variable for the input of acres
           System.out.print("Enter enter the number of inches of rain dropped on average: "); 
                //prompts the user for the average inches of rain dropped by the hurricane.
          double rain = myScanner.nextDouble(); //creates a variable for the input of rain
          
          double inches3 = acres * rain * (6273000); // multiplies acres with rain and converts  to cubic inches.
          double miles3 = inches3 * (0.0000000000000039315); // converts cubic inches to cubic miles
          
          System.out.println( miles3 + " cubic miles."); 

          
          
}  //end of main method   
  	} //end of class


