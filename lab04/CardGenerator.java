// This program randomly generates a card number and suit.
// By: Joseph Malisov
// Feb. 16, 2018
// CSE 2

public class CardGenerator{
  
  public static void main(String[] args) {  //main method

    int card = (int)(Math.random()*52)+1;  //creates a random number between 1 and 52
         
    int v = ((card % 13) + 1) ; //creates a new variable which finds the value of the card from 0 to 12
    String type = "1"; //creates a variable to store the value of the card
    String suit = "1"; //creates a variable to store the suit
    //The following lines convert my initial variable into the four suits
    if (card<=13) {
        suit = "Diamonds"; 
    }
    else if (14<=card && card<=26) {
        suit =  "Clubs"; 
    }
    else if (27<=card && card<=39) {
        suit =  "Hearts";
    }
    else if (40<=card && card<=52) {
        suit =  "Spaids";
    }
    
    //The following lines convert values to types of cards
    switch (v) {
        case 1: type = "Ace";
          break;
        case 11: type = "Jack";
          break;
        case 12: type = "Queen";
          break;
        case 13: type = "King";
          break;
        default: type = Integer.toString(v);
    }
    //The following line prints out which card was picked.
    System.out.println("You picked the " + type + " of " + suit); 

  }
}

